#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/video/video.hpp>

using namespace std;
using namespace cv;

class camera{
public:
    void cap_one_pic();
    void show_one_pic();
    camera(int input);
private:
    Mat pic;
    int i;
    VideoCapture cap;

};
camera::camera(int input)
{
    cap.open(input);
}

void camera::cap_one_pic()
{

    cap.read(pic);
}

void camera::show_one_pic()
{
    imshow("test1",pic);
    cout << "image:" << pic.rows << "," << pic.cols << endl;
    int key=waitKey(0);
}


